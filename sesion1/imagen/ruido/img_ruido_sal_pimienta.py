#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
import matplotlib.pyplot as plt

def salt_pepper_noise(image, s_vs_p, amount):
	row,col = image.shape
	out = image.copy()
	# Modo sal
	num_salt = np.ceil(amount * image.size * s_vs_p)
	coords = [np.random.randint(0, i - 1, int(num_salt))
		      for i in image.shape]
	out[coords] = 1

	# Pepper mode
	num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
	coords = [np.random.randint(0, i - 1, int(num_pepper))
		      for i in image.shape]
	out[coords] = 0
	return out.astype('uint8')

#Cargar imagen
img = cv2.imread('../datos/lenna.png',0)

#Aplicar ruido sal & pimienta
s_vs_p = 0.5
amount = 0.1

noise_img = salt_pepper_noise(img,s_vs_p,amount)

#Aplicar filtro de mediana sobre la imagen
image_conv = cv2.medianBlur(noise_img,5)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original',img)
cv2.waitKey(200)
cv2.imshow('Imagen con ruido sal y pimienta',noise_img)
cv2.waitKey(200)
cv2.imshow('Imagen convolucionada',image_conv)
cv2.waitKey(0)
cv2.destroyAllWindows()
