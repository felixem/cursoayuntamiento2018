#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
import matplotlib.pyplot as plt

def gaussian_noise(image, mean, var):
  row,col= image.shape
  gauss = np.random.normal(mean,var,(row,col))
  gauss = gauss.reshape(row,col)
  noisy = image + gauss
  return noisy.astype('uint8')

#Cargar imagen
img = cv2.imread('../datos/lenna.png',0)

#Aplicar ruido gaussiano
media = 0
varianza = 15

noise_img = gaussian_noise(img,media,varianza)

#Crear matriz de convolución
kernel= np.array([[1,2,1],[2,4,2],[1,2,1]])/16.0
#Imprimir kernel
print kernel

#Aplicar convolución sobre la imagen
image_conv = cv2.filter2D(noise_img,-1,kernel)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original',img)
cv2.waitKey(200)
cv2.imshow('Imagen con ruido gaussiano',noise_img)
cv2.waitKey(200)
cv2.imshow('Imagen convolucionada',image_conv)
cv2.waitKey(0)
cv2.destroyAllWindows()
