#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
from matplotlib import pyplot as plt

#Cargar la imagen
img = cv2.imread('../datos/img-bajo-contraste.png')

# Convertir imagen en escala de grises
img2gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Calcular histograma
hist = cv2.calcHist([img2gray],[0],None,[256],[0,256])

#Ecualizar histograma
equ = cv2.equalizeHist(img2gray)

#Calcular histograma ecualizado
histEqu = cv2.calcHist([equ],[0],None,[256],[0,256])

#Crear ecualizador de histograma adaptativo CLAHE (Contrast Limited Adaptive Histogram Equalization) 
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
img_adap_equ = clahe.apply(img2gray)
#Calcular histograma adaptativo
histAdap = cv2.calcHist([img_adap_equ],[0],None,[256],[0,256])

#Mostrar resultados e imágenes intermedia
cv2.imshow('Imagen original en escala de grises', img2gray)
cv2.waitKey(50)

cv2.imshow('Imagen ecualizada', equ)
cv2.waitKey(50)

cv2.imshow('Imagen ecualizada con CLAHE', equ)
cv2.waitKey(50)

#Mostrar histograma
plt.plot(hist, color='green')
plt.plot(histEqu, color='blue')
plt.plot(histAdap, color='red')
plt.xlim([0,256])
plt.show(block=True)

cv2.destroyAllWindows()
