#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
from matplotlib import pyplot as plt

#Cargar las 2 imágenes
img = cv2.imread('../datos/windmill.jpg')

# Convertir imagen en escala de grises
img2gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Definir umbrales
umbralInferior = 50
umbralSuperior = 200

#Obtener dimensiones de la imagen
h = img2gray.shape[0]
w = img2gray.shape[1]

#Recorrer todos los píxeles de la imagen en gris
for y in range(0, h):
	for x in range(0, w):
		#Asignar valor de gris segun condicion
		img2gray[y, x] = 255 if img2gray[y, x] >= umbralSuperior else 0 if img2gray[y, x] <= umbralInferior else img2gray[y, x]
		
#Corregir colores de la imagen original según las modificaciones en la de gris
imgOut = img.copy()

#Recorrer todos los píxeles de la imagen en gris
for y in range(0, h):
	for x in range(0, w):
		#Asignar valor de gris segun condicion
		imgOut[y, x] = [255, 255, 255] if img2gray[y, x] == 255 else [0, 0, 0] if img2gray[y, x] == 0 else imgOut[y, x]
    

#Calcular histograma
hist = cv2.calcHist([img2gray],[0],None,[256],[0,256])

#Mostrar resultados e imágenes intermedia
cv2.imshow('Imagen original', img)
cv2.waitKey(50)

cv2.imshow('Imagen original umbralizada', imgOut)
cv2.waitKey(50)

cv2.imshow('Imagen en escala de grises umbralizada', img2gray)
cv2.waitKey(50)

#Mostrar histograma
plt.plot(hist)
plt.xlim([0,256])
plt.show(block=True)

cv2.destroyAllWindows()
