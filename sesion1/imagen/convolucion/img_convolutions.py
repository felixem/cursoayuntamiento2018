#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
import matplotlib.pyplot as plt

#Cargar imagen
img = cv2.imread('../datos/grayscale-image.jpg', 0)

#Crear ecualizador de histograma adaptativo CLAHE (Contrast Limited Adaptive Histogram Equalization) 
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
img_eq = clahe.apply(img)


#Crear matriz de convolución
kernelIdentidad = np.array([[0,0,0],[0,1,0],[0,0,0]])
kernelBordes = np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])
kernelSharpen = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
kernelEmboss = np.array([[-2,-1,0],[-1,1,1],[0,1,2]])
kernelMedia = np.array([[1,1,1],[1,1,1],[1,1,1]])/9.0
kernelGauss = np.array([[1,2,1],[2,4,2],[1,2,1]])/16.0

#Elegir kernel
kernel = kernelEmboss
#Imprimir kernel
print kernel
#Aplicar convolución sobre la imagen
image_conv = cv2.filter2D(img_eq,-1,kernel)

#Ecualizar histograma de la imagen
image_conv_equalized = clahe.apply(image_conv)

#Calcular histograma de la imagen original ecualizada
histAdap = cv2.calcHist([img_eq],[0],None,[256],[0,256])
#Calcular histograma de la imagen convolucionada
histConv = cv2.calcHist([image_conv],[0],None,[256],[0,256])
#Calcular histograma de la imagen convolucionada
histConvAdap = cv2.calcHist([image_conv_equalized],[0],None,[256],[0,256])

#Mostrar histograma
plt.plot(histAdap, color='blue')
plt.plot(histConv, color='red')
plt.plot(histConvAdap, color='green')
plt.xlim([0,256])
plt.show(block=False)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original',img)
cv2.waitKey(200)
cv2.imshow('Imagen original ecualizada',img_eq)
cv2.waitKey(200)
cv2.imshow('Imagen convolucionada',image_conv)
cv2.waitKey(200)
cv2.imshow('Imagen convolucionada y ecualizada',image_conv_equalized)
cv2.waitKey(0)
cv2.destroyAllWindows()
