#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

BLUE = [255,0,0]

#Cargar imagen
img1 = cv2.imread('../datos/windmill.jpg')

#Borde en el que se repite el último elemento (aaaaaa|abcdefgh|hhhhhhh)
replicate = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_REPLICATE)
#Borde en el que se reflejan los elementos situados en los bordes de los objetos (fedcba|abcdefgh|hgfedcb)
reflect = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_REFLECT)
#Borde similar al anterior, pero con un ligero cambio (gfedcb|abcdefgh|gfedcba)
reflect101 = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_REFLECT_101)
#Borde que replica los bordes opuestos (cdefgh|abcdefgh|abcdefg)
wrap = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_WRAP)
#Borde de color constante
constant= cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_CONSTANT,value=BLUE)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original',img1)
cv2.waitKey(50)
cv2.imshow('Border replicate',replicate)
cv2.waitKey(50)
cv2.imshow('Border reflect',reflect)
cv2.waitKey(50)
cv2.imshow('Border reflect_101',reflect101)
cv2.waitKey(50)
cv2.imshow('Border wrap',wrap)
cv2.waitKey(50)
cv2.imshow('Border constant',constant)
cv2.waitKey(0)
cv2.destroyAllWindows()
