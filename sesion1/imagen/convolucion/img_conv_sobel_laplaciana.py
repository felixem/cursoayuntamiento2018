#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
import math

#Cargar imagen
img = cv2.imread('../datos/grayscale-image.jpg', 0)

#Crear ecualizador de histograma adaptativo CLAHE (Contrast Limited Adaptive Histogram Equalization) 
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
img_eq = clahe.apply(img)

#Crear matriz de convolución
kernelSobelX = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernelSobelY = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
kernelLaplaciana = np.array([[0,1,0],[1,-4,1],[0,1,0]])

#Aplicar convoluciones sobre la imagen
image_sobelX = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3)
image_sobelY = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3)
image_laplaciana = cv2.Laplacian(img,cv2.CV_64F)

#Convertir a formato 8u sin perder gradientes negativos
abs_sobelX64f = np.absolute(image_sobelX)
image_sobelX = np.uint8(abs_sobelX64f)
abs_sobelY64f = np.absolute(image_sobelY)
image_sobelY = np.uint8(abs_sobelY64f)
abs_laplaciana64f = np.absolute(image_laplaciana)
image_laplaciana = np.uint8(abs_laplaciana64f)

#image_sobelX = cv2.filter2D(img_eq,-1,kernelSobelX)
#image_sobelY = cv2.filter2D(img_eq,-1,kernelSobelY)
#image_laplaciana = cv2.filter2D(img_eq,-1,kernelLaplaciana)

#Magnitud del gradiente
#image_sobel = math.sqrt(image_sobelX**2 + image_sobelY**2)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original',img)
cv2.waitKey(200)
cv2.imshow('Imagen original ecualizada',img_eq)
cv2.waitKey(200)
cv2.imshow('Imagen sobel X',image_sobelX)
cv2.waitKey(200)
cv2.imshow('Imagen sobel Y',image_sobelY)
cv2.waitKey(200)
cv2.imshow('Imagen laplaciana',image_laplaciana)
cv2.waitKey(0)
cv2.destroyAllWindows()
