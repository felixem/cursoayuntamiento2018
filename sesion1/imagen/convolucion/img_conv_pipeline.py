#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
import matplotlib.pyplot as plt

#Cargar imagen
img = cv2.imread('../datos/grayscale-image.jpg', 0)

#Crear ecualizador de histograma adaptativo CLAHE (Contrast Limited Adaptive Histogram Equalization) 
clahe = cv2.createCLAHE(clipLimit=0.03, tileGridSize=(8,8))

#Crear matriz de convolución
kernelBordes = np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])
kernelSharpen = np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])
kernelMedia = np.array([[1,1,1],[1,1,1],[1,1,1]])/9.0

#Aplicar sharpen sobre la imagen
image_sharpen = cv2.filter2D(img,-1,kernelSharpen)

#Aplicar detector de bordes
image_bordes = cv2.filter2D(image_sharpen,-1, kernelBordes)
#Aplicar filtro de media
image_denoised = cv2.filter2D(image_bordes,-1, kernelMedia)

#Ecualizar histograma de la imagen
image_out = clahe.apply(image_denoised)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original',img)
cv2.waitKey(200)
cv2.imshow('Imagen con bordes realzados',image_sharpen)
cv2.waitKey(200)
cv2.imshow('Imagen con deteccion de bordes',image_bordes)
cv2.waitKey(200)
cv2.imshow('Imagen final tras reducir el ruido con filtro de media',image_denoised)
cv2.waitKey(200)
cv2.imshow('Imagen final tras ecualizar el histograma',image_out)
cv2.waitKey(0)
cv2.destroyAllWindows()
