#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import math

#Cargar imagen
img = cv2.imread('../datos/cara.png', 0)

#Elegir filtro
#INTER_NEAREST - a nearest-neighbor interpolation
#INTER_LINEAR - a bilinear interpolation (used by default)
#INTER_AREA - resampling using pixel area relation. It may be a preferred method for image decimation, as it gives moire’-free results. But when the image is zoomed, it is similar to the INTER_NEAREST method.
#INTER_CUBIC - a bicubic interpolation over 4x4 pixel neighborhood
#INTER_LANCZOS4 - a Lanczos interpolation over 8x8 pixel neighborhood

filtro_downsampling = cv2.INTER_AREA
filtro_upsampling = cv2.INTER_NEAREST

#Elegir factor de  crecimiento tamaño
factor = 0.25

#Aplicar filtro de downsampling
img_downsampled = cv2.resize(img, (0,0), fx=factor, fy=factor, interpolation=filtro_downsampling)

#Aplicar filtro de upsampling
factorInv = 1.0/factor
img_upsampled= cv2.resize(img_downsampled, (0,0), fx=factorInv, fy=factorInv, interpolation=filtro_upsampling)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original 128x128',img)
cv2.waitKey(200)
cv2.imshow('Imagen downsampleada 32x32',img_downsampled)
cv2.waitKey(200)
cv2.imshow('Imagen upsampleada 128x128',img_upsampled)
cv2.waitKey(0)
cv2.destroyAllWindows()
