#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

#Cargar imagen
img = cv2.imread('../datos/windmill.jpg')
#Acceder a los valores del pixel de la posicion (100,100)
px = img[100,100]
print px

#Acceder solo al componente azul del pixel
blue = img[100,100,0]
print blue

#Modificar el color de los 3 componentes del pixel
img[100,100] = [255,255,255]
print img[100,100]

#Modificar el color del componente rojo del pixel
img[100,100, 2] = 0
print img[100,100]

