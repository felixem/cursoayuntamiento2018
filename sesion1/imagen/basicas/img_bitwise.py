#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

#Cargar las 2 imágenes
img1 = cv2.imread('../datos/ml.png')
img2 = cv2.imread('../datos/opencv-logo-resized.png')

# Crear una máscara del logo y su inversa
img2gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
#Crear una imagen binaria en la que se distinga la parte perteneciente al logo de la parte de fondo
ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
#Invertir para que la parte del logo quede en negro (para oscurecer la imagen donde se insertará el logo)
mask_inv = cv2.bitwise_not(mask)

# Oscurecer (poner a 0) la zona de la primera imagen donde se va a insertar el logo
img1_bg = cv2.bitwise_and(img1,img1,mask = mask_inv)

# Obtener solamente la región del logo de la imagen del logo
img2_fg = cv2.bitwise_and(img2,img2,mask = mask)

# Insertar el logo en la primera imagen
dst = cv2.add(img1_bg,img2_fg)

#Mostrar resultados e imágenes intermedia
cv2.imshow('Mascara del logo',mask)
cv2.waitKey(50)

cv2.imshow('Mascara del logo invertida',mask_inv)
cv2.waitKey(50)

cv2.imshow('Imagen 1 con la zona del logo oscurecida',img1_bg)
cv2.waitKey(50)

cv2.imshow('Logo aislado',img2_fg)
cv2.waitKey(50)

cv2.imshow('Resultado',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
