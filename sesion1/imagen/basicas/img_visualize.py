#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

#Cargar imagen
img = cv2.imread('../datos/windmill.jpg')
#Mostrar dimensiones de la imagen
print "Dimensiones de la imagen: " + str(img.shape)
#Número total de píxeles 
print "Píxeles de la imagen: " + str(img.size)
#Mostrar imagen en la ventana
cv2.imshow('Imagen',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
