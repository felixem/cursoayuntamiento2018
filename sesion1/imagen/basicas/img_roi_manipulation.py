#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

#Cargar imagen
img = cv2.imread('../datos/windmill.jpg')
#Obtener un trozo de la imagen
molino = img[0:600, 400:800]
#Manipular un trozo de la imagen original copiando el trozo obtenido anteriormente
img[0:600, 0:400] = molino

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen original modificada',img)
cv2.waitKey(50)
cv2.imshow('Region de la imagen',molino)
cv2.waitKey(0)
cv2.destroyAllWindows()
