#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

#Cargar imágenes
img1 = cv2.imread('../datos/ml.png')
img2 = cv2.imread('../datos/opencv-logo-resized.png')

#Suma ponderada
dst = cv2.addWeighted(img1,0.7,img2,0.3,0)

#Mostrar imagen
cv2.imshow('dst',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
