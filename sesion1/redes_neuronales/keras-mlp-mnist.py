#!/usr/bin/python
# -*- coding: latin-1 -*-

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt

# # Cargar datos
# 
# Keras viene con un cargador de datos de MNIST. Kerastiene la función `mnist.load_data()` que descarga los datos de sus servidores si no se encuentran en local.
# Los datos se cargan separados en conjunto de entrenamiento y de test.

from keras.datasets import mnist
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

# # Vistazo a los datos
# 
# Los datos consisten en números escritos a mano entre 0 y 9, junto con su ground truth (valor numérico que representa). Tienes 60.000 ejemplos de entrenamiento y 10000 ejemplos de test. Cada ejemplo es una imagen de grises de 28x28.

from keras.utils import to_categorical

print('Training data shape : ', train_images.shape, train_labels.shape)
print('Testing data shape : ', test_images.shape, test_labels.shape)

# Encontrar la cantidad de clases únicas a partir de las etiquetas de entrenamiento
classes = np.unique(train_labels)
nClasses = len(classes)
print('Total number of outputs : ', nClasses)
print('Output classes : ', classes)

plt.figure(figsize=[10,5])

# Mostrar la primera imagen del conjunto de entrenamiento
plt.subplot(121)
plt.imshow(train_images[0,:,:], cmap='gray')
plt.title("Ground Truth : {}".format(train_labels[0]))

# Mostrar la primera imagen del conjunto de test
plt.subplot(122)
plt.imshow(test_images[0,:,:], cmap='gray')
plt.title("Ground Truth : {}".format(test_labels[0]))
plt.show(block=True)


# # Procesar los datos
# 
# * Las imágenes están en escala de grises y sus valores oscilan entre 0 y 255.
# * Convertir cada matriz de imagen 28x28 en un vector de 784 dimensiones que se meterá como entrada de la red.
# * Convertimos los datos a flotante y escalamos los valores entre 0 y 1.
# * Además, convertirmos las etiquetas a la codificación hot-one categórica necesaria para que Keras realice una clasificación multiclase.

# Cambiar la matriz de 28x28 a un array de dimensión 784.
dimData = np.prod(train_images.shape[1:])
train_data = train_images.reshape(train_images.shape[0], dimData)
test_data = test_images.reshape(test_images.shape[0], dimData)

# Cambiar el tipo de dato a flotante
train_data = train_data.astype('float32')
test_data = test_data.astype('float32')

# Escalar los datos para que caigan entre 0 y 1.
train_data /= 255
test_data /= 255

# Cambiar las etiquetas a datos categóricos (one-hot)
train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)

# Mostrar el cambio de una etiqueta de categoría a su codificación one-hot.
print('Original label 0 : ', train_labels[0])
print('After conversion to categorical ( one-hot ) : ', train_labels_one_hot[0])

# # Crear la red
from keras.models import Sequential
from keras.layers import Dense
model = Sequential()
#Capa de entrada con 512 neuronas y activación relu
model.add(Dense(512, activation='relu', input_shape=(dimData,)))
#Capa oculta con 512 neuronas y activación relu
model.add(Dense(512, activation='relu'))
#Capa final con tantas salidas como clases posibles hay (0-9), con función de activación softmax (probabilidades que suman 1)
model.add(Dense(nClasses, activation='softmax'))

# # Configurar the Network

#Indicar el algoritmo de optimización, la función de error y la métrica que a la que queremos hacer seguimiento durante el proceso
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])


# # Entrenar la red

#La red está preparada para ser entrenada. Especificamos el número de épocas a 20, lo que significa que se harán 20 pasadas completas sobre el dataset completo.
#El tamaño de batch indica el tamaño de las agrupaciones de los datos de entrada, de manera que se actualizan los pesos una vez por batch, tras haber procesado todos los datos de entrada pertenecientes al mismo
history = model.fit(train_data, train_labels_one_hot, batch_size=256, epochs=8, verbose=1, 
                   validation_data=(test_data, test_labels_one_hot))

# # Mostrar la pérdida y las curvas de precisión
plt.figure(figsize=[8,6])
plt.plot(history.history['loss'],'r',linewidth=3.0)
plt.plot(history.history['val_loss'],'b',linewidth=3.0)
plt.legend(['Training loss', 'Validation Loss'],fontsize=18)
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Loss',fontsize=16)
plt.title('Loss Curves',fontsize=16)
plt.show(block=True)

plt.figure(figsize=[8,6])
plt.plot(history.history['acc'],'r',linewidth=3.0)
plt.plot(history.history['val_acc'],'b',linewidth=3.0)
plt.legend(['Training Accuracy', 'Validation Accuracy'],fontsize=18)
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Accuracy',fontsize=16)
plt.title('Accuracy Curves',fontsize=16)
plt.show(block=True)


# # Evaluar la red entrenada sobre los datos de test
[test_loss, test_acc] = model.evaluate(test_data, test_labels_one_hot)
print("Evaluation result on Test Data : Loss = {}, accuracy = {}".format(test_loss, test_acc))


# # Crear una nueva red con regularización dropout, consistente en desactivar la salida de ciertas neuronas con una probabilidad p.
# De esta manera se trata de conseguir que no hayan neuronas "muertas".
from keras.layers import Dropout
model_reg = Sequential()
#Capa de entrada
model_reg.add(Dense(512, activation='relu', input_shape=(dimData,)))
#Capa de regularización
model_reg.add(Dropout(0.5))
#Capa oculta
model_reg.add(Dense(512, activation='relu'))
#Capa de regularización
model_reg.add(Dropout(0.5))
#Capa de salida con activación softmax
model_reg.add(Dense(nClasses, activation='softmax'))

model_reg.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

history_reg = model_reg.fit(train_data, train_labels_one_hot, batch_size=256, epochs=8, verbose=1, 
                            validation_data=(test_data, test_labels_one_hot))

plt.figure(figsize=[8,6])
plt.plot(history_reg.history['loss'],'r',linewidth=3.0)
plt.plot(history_reg.history['val_loss'],'b',linewidth=3.0)
plt.legend(['Training loss', 'Validation Loss'],fontsize=18)
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Loss',fontsize=16)
plt.title('Loss Curves',fontsize=16)
plt.show(block=True)


plt.figure(figsize=[8,6])
plt.plot(history_reg.history['acc'],'r',linewidth=3.0)
plt.plot(history_reg.history['val_acc'],'b',linewidth=3.0)
plt.legend(['Training Accuracy', 'Validation Accuracy'],fontsize=18)
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Accuracy',fontsize=16)
plt.title('Accuracy Curves',fontsize=16)
plt.show(block=True)

[test_loss, test_acc] = model_reg.evaluate(test_data, test_labels_one_hot)
print("Evaluation result on Test Data : Loss = {}, accuracy = {}".format(test_loss, test_acc))


# # Predict the first image from test data
# 
# We have seen that the first image is the number 7. Let us predict using the model

# Mostrar la primera imagen del conjunto de test
plt.subplot(122)
plt.imshow(test_images[0,:,:], cmap='gray')
plt.title("Ground Truth : {}".format(test_labels[0]))
plt.show(block=True)

#Realizar predicciones sobre la imagen de test
print ('Clase esperada :' + str(test_labels[0]))

# Predicir la clase más probable
print ('Clase predicha:')
print (model_reg.predict_classes(test_data[[0],:]))

# Predecir las probabilidades para cada clase
print ('Probabilidades para todas las clases:')
print (model_reg.predict(test_data[[0],:]))



