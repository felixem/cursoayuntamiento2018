#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

SZ = 20

def split2d(img, cell_size, flatten=True):
    h, w = img.shape[:2]
    sx, sy = cell_size
    cells = [np.hsplit(row, w//sx) for row in np.vsplit(img, h//sy)]
    cells = np.array(cells)
    if flatten:
        cells = cells.reshape(-1, sy, sx)
    return cells

def load_digits(fn):
    digits_img = cv2.imread(fn, 0)
    digits = split2d(digits_img, (SZ, SZ))
    return digits, digits_img

def deskew(img):
    m = cv2.moments(img)
    if abs(m['mu02']) < 1e-2:
        # no deskewing needed. 
        return img.copy()
    # Calculate skew based on central momemts. 
    skew = m['mu11']/m['mu02']
    # Calculate affine transform to correct skewness. 
    M = np.float32([[1, skew, -0.5*SZ*skew], [0, 1, 0]])
    # Apply affine transform
    img = cv2.warpAffine(img, M, (SZ, SZ), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR)
    return img

#Cargar digitos
digits, digits_img = load_digits('../datos/digits.png')

#Corregir giros
digits_deskewed = list(map(deskew, digits))

#Iterar sobre los digitos
num = 10
val = min(num, len(digits_deskewed)-1)

for x in range(0, val) :
	cv2.imshow('Digito '+str(x),digits_deskewed[x])
	cv2.waitKey(100)

#Mostrar imagen
cv2.imshow('Imagen con los digitos originales',digits_img)
cv2.waitKey(0)
cv2.destroyAllWindows()
