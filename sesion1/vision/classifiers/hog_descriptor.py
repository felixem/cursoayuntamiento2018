#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

SZ = 20

def split2d(img, cell_size, flatten=True):
    h, w = img.shape[:2]
    sx, sy = cell_size
    cells = [np.hsplit(row, w//sx) for row in np.vsplit(img, h//sy)]
    cells = np.array(cells)
    if flatten:
        cells = cells.reshape(-1, sy, sx)
    return cells

def load_digits(fn):
    digits_img = cv2.imread(fn, 0)
    digits = split2d(digits_img, (SZ, SZ))
    return digits, digits_img

def deskew(img):
    m = cv2.moments(img)
    if abs(m['mu02']) < 1e-2:
        # no deskewing needed. 
        return img.copy()
    # Calculate skew based on central momemts. 
    skew = m['mu11']/m['mu02']
    # Calculate affine transform to correct skewness. 
    M = np.float32([[1, skew, -0.5*SZ*skew], [0, 1, 0]])
    # Apply affine transform
    img = cv2.warpAffine(img, M, (SZ, SZ), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR)
    return img

#Cargar digitos
digits, digits_img = load_digits('../datos/digits.png')

#Corregir giros
digits_deskewed = list(map(deskew, digits))

#Parámetros de hog
#Tamaño del dígito del dataset. 20x20 en nuestro caso.
winSize = (20,20)
#Tamaño del bloque para normalizar los histogramas y paliar las variaciones de iluminación
blockSize = (10,10)
#Determina el solapamiento de los bloques vecinos y controla el grado de normalización del contraste.
blockStride = (5,5)
#El tamaño de la celda es elegido basándose en la escala de las características importantes para la clasificación
#Un tamaño de celda muy pequeño haría enorme el tamaño del vector de característas, y uno muy grande no capturaría información relevante.
cellSize = (10,10)
#Número de partes en los que se divide el histograma de gradientes
nbins = 9
#Típicamente los gradientes pueden tener cualquier orientación entre 0 y 360 grados, estos son los gradientes con signo. 
#Los gradientes sin signo toman valores entre 0 y 180 grados.
signedGradients = True
derivAperture = 1
winSigma = -1.
histogramNormType = 0
L2HysThreshold = 0.2
gammaCorrection = 1
nlevels = 64

#Instanciar descriptor
hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, signedGradients)

#Iterar sobre los digitos
num = 10
val = min(num, len(digits_deskewed)-1)

for x in range(0, val) :
	#Calcular descriptor
	descriptor = hog.compute(digits_deskewed[x])
	print('Descriptor: '+str(descriptor))
	cv2.imshow('Digito '+str(x),digits_deskewed[x])
	cv2.waitKey(100)

#Mostrar imagen
cv2.imshow('Imagen con los digitos originales',digits_img)
cv2.waitKey(0)
cv2.destroyAllWindows()
