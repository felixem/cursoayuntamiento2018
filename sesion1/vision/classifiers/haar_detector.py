#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np

#Cargar clasificadores entrenados
face_cascade = cv2.CascadeClassifier('../datos/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('../datos/haarcascade_eye.xml')

#Cargar imagen
img = cv2.imread('../datos/facial_expressions.png')
#Canvertir a gris
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#Detectar caras en varias escalas
faces = face_cascade.detectMultiScale(gray, 1.3, 5)
#Dibujar rectángulo sobre las caras
for (x,y,w,h) in faces:
    img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    roi_gray = gray[y:y+h, x:x+w]
    roi_color = img[y:y+h, x:x+w]
    #Buscar si hay ojos dentro de la región
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex,ey,ew,eh) in eyes:
    	#Dibujar rectángulo sobre los ojos
        cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

#Mostrar imagen
cv2.imshow('Resultado',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
