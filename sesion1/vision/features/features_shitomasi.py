#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import math
import numpy as np

#Cargar imagen
img = cv2.imread('../datos/simple-forms.jpg')

#Convertir a gris
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Formatear a float 32
gray = np.float32(gray)

#Aplicar detector de esquinas de Harris
corners = cv2.goodFeaturesToTrack(gray,25,0.01,10)

corners = np.int0(corners)

for i in corners:
    x,y = i.ravel()
    cv2.circle(img,(x,y),3,(0,0,255),-1)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen con deteccion de esquinas',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
