#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import math
import numpy as np

#Cargar imagen
img = cv2.imread('../datos/simple-forms.jpg')

#Convertir a gris
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Formatear a float 32
gray = np.float32(gray)

#Aplicar detector de esquinas de Harris
dst = cv2.cornerHarris(gray,2,3,0.04)

#Aplicar dilatación para destacar las esquinas (no es importante)
dst = cv2.dilate(dst,None)

# Umbral para obtener el valor óptimo. Puede variar dependiendo de la imagen
img[dst>0.01*dst.max()]=[0,0,255]

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen con deteccion de esquinas',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
