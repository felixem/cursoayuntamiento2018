#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import math
import numpy as np

#Cargar imagen
img = cv2.imread('../datos/simple-forms.jpg')

#Convertir a gris
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Instanciar detector
surf = cv2.xfeatures2d.SURF_create(500)
#surf.setHessianThreshold = 500

#Detectar puntos
kp, des = surf.detectAndCompute(gray,None)

#Dibujar puntos
cv2.drawKeypoints(gray,kp,img, (255,0,0),4)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen con deteccion de keypoints con orientacion y escala',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
