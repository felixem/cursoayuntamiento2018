#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import math
import numpy as np

#Cargar imagen
img = cv2.imread('../datos/simple-forms.jpg')

#Convertir a gris
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

#Instanciar detector
#sift = cv2.SIFT()
sift = cv2.xfeatures2d.SIFT_create()
kp = sift.detect(gray,None)

#Calcular descriptores
#kp,des = sift.compute(gray,kp)

#Dibujar puntos
cv2.drawKeypoints(gray,kp,img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen con deteccion de keypoints con orientacion y escala',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
