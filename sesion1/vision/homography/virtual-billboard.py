#!/usr/bin/python
# -*- coding: latin-1 -*-

import cv2
import numpy as np
import sys

def mouse_handler(event, x, y, flags, data) :
    
    if event == cv2.EVENT_LBUTTONDOWN :
        cv2.circle(data['im'], (x,y),3, (0,0,255), 5, 16);
        cv2.imshow("Image", data['im']);
        if len(data['points']) < 4 :
            data['points'].append([x,y])

def get_four_points(im):
    
    #Preparar los datos a enviar al manejador del ratón
    data = {}
    data['im'] = im.copy()
    data['points'] = []
    
    #Vincular la función de callback
    cv2.imshow("Image",im)
    cv2.setMouseCallback("Image", mouse_handler, data)
    cv2.waitKey(0)
    
    # Convertir array
    points = np.vstack(data['points']).astype(float)
    return points


#Leer imagen fuente.
im_src = cv2.imread('../datos/first-image.jpg');
size = im_src.shape

# Crear vector de puntos.
pts_src = np.array(
                   [
                    [0,0],
                    [size[1] - 1, 0],
                    [size[1] - 1, size[0] -1],
                    [0, size[0] - 1 ]
                    ],dtype=float
                   );


# Leer imagen destino
im_dst = cv2.imread('../datos/times-square.jpg');

# Obtener las 4 esquinas donde colocar la imagen fuente
print 'Elige las cuatro esquinas de la zona donde colocar la imagen (esquina superior izquierda - esquina superior derecha - esquina inferior derecha - esquina inferior izquierda) y presionar ENTER'
pts_dst = get_four_points(im_dst)

# Calcular la homografía entre los puntos fuente y destino
h, status = cv2.findHomography(pts_src, pts_dst);

# Trasnformar imagen fuente
im_temp = cv2.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))

#Copiar imagen destino
im_out = im_dst.copy()

# Convertir a negro la zona donde colocar la imagen
cv2.fillConvexPoly(im_out, pts_dst.astype(int), 0, 16);

# Añadir la imagen fuente al destino.
im_out = im_out + im_temp;

# Mostrar imagen.
cv2.imshow("Imagen fuente", im_src);
cv2.waitKey(110);
cv2.imshow("Imagen destino", im_dst);
cv2.waitKey(110);
cv2.imshow("Imagen final", im_out);
cv2.waitKey(0);

