#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
import cv2
from matplotlib import pyplot as plt

#Cargar imágenes
img2 = cv2.imread('../datos/grand_canyon_left_01.png') 
img1 = cv2.imread('../datos/grand_canyon_right_01.png')

# Iniciar detector SURF
surf = cv2.xfeatures2d.SURF_create()

# Encontrar keypoints y descriptores
kp1, des1 = surf.detectAndCompute(img1,None)
kp2, des2 = surf.detectAndCompute(img2,None)

# Encontrar emparejamientos con distancia euclídea
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1,des2, k=2)

# Guardar todos los buenos emparejamientos
good = []
for m,n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

#Número mínimo de puntos de correspondencia
MIN_MATCH_COUNT = 10

#Comprobar si hay suficientes puntos
if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

	#Calcular la matriz de perspectiva para encajarla primera imagen con la segunda aplicando RANSAC
    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()
    
    #Realizar la proyección de la primera imagen para juntarla con la segunda y formar la panorámica
    img3 = cv2.warpPerspective(img1, M, (img1.shape[1] + img2.shape[1], img1.shape[0]))
    img3[0:img2.shape[0], 0:img2.shape[1]] = img2

else:
    print "No se han encontrado suficientes emparejamientos - %d/%d" % (len(good),MIN_MATCH_COUNT)
    
#Criterio de dibujar emparejamientos
draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)

#Dibujar emparejamientos
img_emp = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

#Mostrar imágenes en las ventanas
cv2.imshow('Imagen origen',img1)
cv2.waitKey(100)
cv2.imshow('Imagen destino',img2)
cv2.waitKey(100)
cv2.imshow('Emparejamientos',img_emp)
cv2.waitKey(100)
cv2.imshow('Imagen panoramica',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()
