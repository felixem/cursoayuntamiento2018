#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
import cv2
from matplotlib import pyplot as plt

#Cargar imágenes
img1 = cv2.imread('../datos/box.png',0) 
img2 = cv2.imread('../datos/box_in_scene.png',0)

# Iniciar detector SURF
surf = cv2.xfeatures2d.SURF_create()

# Encontrar keypoints y descriptores
kp1, des1 = surf.detectAndCompute(img1,None)
kp2, des2 = surf.detectAndCompute(img2,None)

# Encontrar emparejamientos con distancia euclídea
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1,des2, k=2)

# Guardar todos los buenos emparejamientos
good = []
for m,n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

#Número mínimo de puntos de correspondencia
MIN_MATCH_COUNT = 10

#Comprobar si hay suficientes puntos
if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

	#Calcular la matriz de perspectiva para encajar el objeto de la primera imagen con el de la segunda aplicando RANSAC
    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()
	#Obtener borde de la imagen 1
    h,w = img1.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    #Aplicar transformación de perspectiva
    dst = cv2.perspectiveTransform(pts,M)
	
	#Dibujar borde de la imagen original con la perspectiva de la segunda imagen
    img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)

else:
    print "No se han encontrado suficientes emparejamientos - %d/%d" % (len(good),MIN_MATCH_COUNT)
    matchesMask = None

#Criterio de dibujar emparejamientos
draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)

#Dibujar emparejamientos
img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

#Mostrar imágenes en las ventanas
cv2.imshow('Correspondencias y homografia',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()
