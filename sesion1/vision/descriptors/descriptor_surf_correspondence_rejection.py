#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
import cv2
from matplotlib import pyplot as plt

#Cargar imágenes
img1 = cv2.imread('../datos/box.png',0) 
img2 = cv2.imread('../datos/box_in_scene.png',0)

# Iniciar detector SURF
surf = cv2.xfeatures2d.SURF_create()

# Encontrar keypoints y descriptores
kp1, des1 = surf.detectAndCompute(img1,None)
kp2, des2 = surf.detectAndCompute(img2,None)

# Encontrar emparejamientos con distancia euclídea
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1,des2, k=2)

# Máscara para buenos emparejamientos
matchesMask = [[0,0] for i in xrange(len(matches))]

# Test ratio según el paper de Lowe
for i,(m,n) in enumerate(matches):
    if m.distance < 0.7*n.distance:
        matchesMask[i]=[1,0]

#Función para definir como se dibujan los emparejamientos
draw_params = dict(matchColor = (0,255,0),
                   singlePointColor = (255,0,0),
                   matchesMask = matchesMask,
                   flags = 0)

#Dibujar emparejamientos
img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,matches,None,**draw_params)

#Mostrar imágenes en las ventanas
cv2.imshow('Emparejamientos filtrados',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()
