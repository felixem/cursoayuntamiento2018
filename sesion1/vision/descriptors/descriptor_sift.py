#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
import cv2
from matplotlib import pyplot as plt

#Cargar imágenes
img1 = cv2.imread('../datos/box.png',0) 
img2 = cv2.imread('../datos/box_in_scene.png',0)

# Iniciar detector SIFT
sift = cv2.xfeatures2d.SIFT_create()

# Encontrar keypoints y descriptores
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

# Encontrar emparejamientos con distancia euclídea
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1,des2, k=1)

# Lista de buenos emparejamientos
good = []
for m in matches:
	good.append(m)

# cv2.drawMatchesKnn expects list of lists as matches.
img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=2)

#Mostrar imágenes en las ventanas
cv2.imshow('Emparejamientos sin filtrar',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()
