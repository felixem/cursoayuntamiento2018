#!/usr/bin/python
# -*- coding: latin-1 -*-

#sudo pip install PyWavelets

import numpy as np
import pywt
import cv2

#Cargar imagen en escala de grises
img = cv2.imread('../datos/Largehaarimage.jpg',0)

#Definir nivel
level =  1

#Convertir a flotante
imgF =  np.float32(img)   
imgF /= 255.0;

#Calcular transformación de haar discreta
res = pywt.wavedec2(imgF, wavelet='haar', mode='symmetric', level = level)
out = res[0]

#Ajustar valores de intensidad
out = out / (2.0 ** level)

#Transformar al rango 0-255
out *= 255.0;
out =  np.uint8(out)

#Mostrar resultado
cv2.imshow('Imagen inicial',img)
cv2.waitKey(100)
cv2.imshow('Resultado',out)
cv2.waitKey(100)

#Imágenes de diferencias
val = 0
for x in range(1, level+1):
	for y in range (0,3):
		#realLvl = level + 1 - x
		imgHaar = res[x][y]
		#imgHaar = imgHaar/ (2.0 ** realLvl)
		cv2.imshow('Haar '+str(val),imgHaar)
		cv2.waitKey(100)
		val = val + 1

cv2.waitKey(0)
cv2.destroyAllWindows()
