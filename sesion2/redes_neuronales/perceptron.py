#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
from matplotlib import pyplot as plt
import random
    
#Evaluar salida del perceptron
def eval_perceptron(w, x):
    if np.dot(x, w) > 0:
        out = 1
    else:
        out = -1
    return out    
    
def perceptron_train_plot(X, Y):
    '''
    Entrena el perceptron y muestra el error total en cada época.
    
    :param X: datos de ejemplo
    :param Y: etiquetas de los datos
    :return: vector de pesos en forma de array
    '''
    w = np.zeros(len(X[0]))
    
    eta = 1
    n = 30
    errors = []
    
    counter = 0
    c = list(zip(X, Y))

    for t in range(n):
        #Ordenar aleatoriamente los conjuntos
        random.shuffle(c)
        Xaux, Yaux = zip(*c)
        
        #Conteo del error
        total_error = 0
        for i, x in enumerate(Xaux):
            #Evaluar perceptron
            res = eval_perceptron(w, Xaux[i])
            counter = counter + 1
            #Comprobar clasificacion erronea
            if (res*Yaux[i] <= 0):
                total_error += 1
                #Actualizar pesos
                w = w + eta*Xaux[i]*Yaux[i]
        
        #Añadir error al historial
        errors.append(total_error)
        
    plt.plot(errors)
    plt.xlabel('Epoca')
    plt.ylabel('Error total')
    plt.show()
    
    return w

#Conjunto de datos de entrenamiento (el último elemento es necesario para multiplicarlo por el bias)
X = np.array([
    [-2.0,4.0,1.0],
    [-2.0,1.0,1.0],
    [1.0, -2.0, 1.0],
    [1.0, 6.0, 1.0],
    [2.0, 4.0, 1.0],
    [6.0, 2.0, 1.0],
])

#Clases a las que pertenece cada elemento
y = np.array([-1.0,-1.0,-1.0, 1.0,1.0,1.0])

for i in range(0, len(X)):
    #Comprobar si es ejemplo positivo o negativo
    sample = X[i]
    if y[i] == -1:
        plt.scatter(sample[0], sample[1], s=120, marker='_', linewidths=2)
    else:
        plt.scatter(sample[0], sample[1], s=120, marker='+', linewidths=2)

#Mostrar puntos
plt.show()

#Calcular los pesos del perceptron para clasificar el conjunto
w = perceptron_train_plot(X,y)
print(w)

#Datos de test
Xtest = np.array([
    [-2.0,1.0,-1.0],
    [4.0,4.0,-1.0],
])

ytest = np.array([-1,1])

# Mostrar la frontera de decision
# Crear malla donde mostrar la salida
h = .05
x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                     np.arange(y_min, y_max, h))

# Mostrar la frontera de decision
fig, ax = plt.subplots()

Z = np.array([])
ix = xx.ravel()
iy = yy.ravel()

inputs = np.c_[ix, iy]

#Calcular activaciones en cada parte de la malla
for i in range(0,len(inputs)):
    entrada = np.array([inputs[i][0], inputs[i][1], 1])
    #print entrada
    res = eval_perceptron(w, entrada)
    #print res
    Z = np.append(Z, res)

# Mostrar el resultado coloreado
Z = Z.reshape(xx.shape)
ax.contourf(xx, yy, Z, cmap=plt.cm.Paired)
ax.axis('off')

#Código de colores para mostrar
colores = []
for i in range(0, len(y)):
    if y[i] == -1:
        colores.append('blue')
    else:
        colores.append('orange')


# Mostrar los puntos de entrenamiento
ax.scatter(X[:, 0], X[:, 1], c=colores, cmap=plt.cm.Paired)

ax.set_title('Perceptron')

#Mostrar grafica
plt.show(block=True)
plt.close()


