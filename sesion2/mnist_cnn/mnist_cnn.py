import numpy
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
K.set_image_dim_ordering('th')
from scipy import misc
import matplotlib.pyplot as plt

# fix random seed for reproducibility
seed = 7
numpy.random.seed(seed)

# load data
(X_train, y_train), (X_test, y_test) = mnist.load_data()
# reshape to be [samples][pixels][width][height]
X_train = X_train.reshape(X_train.shape[0], 1, 28, 28).astype('float32')
X_test = X_test.reshape(X_test.shape[0], 1, 28, 28).astype('float32')

# normalize inputs from 0-255 to 0-1
X_train = X_train / 255
X_test = X_test / 255
# one hot encode outputs
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_test.shape[1]

def baseline_model():
	# create model
	model = Sequential()
	model.add(Conv2D(32, (5, 5), input_shape=(1, 28, 28), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Dropout(0.2))
	model.add(Flatten())
	model.add(Dense(128, activation='relu'))
	model.add(Dense(num_classes, activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

# build the model
model = baseline_model()
# Fit the model
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=1)

# Loading weights file (a pretrained model)
# model.load_weights("model.h5")

# Final evaluation of the model
scores = model.evaluate(X_test, y_test, verbose=0)
print("CNN Error: %.2f%%" % (100-scores[1]*100))

#model.save_weights("model.h5")

sample = X_test[0].reshape((1,1,28,28))
scores = model.predict(sample, verbose=0)
print("Ground Truth "+str(y_test[0]))
print("Predicted "+str(scores))

plt.figure(figsize=[10,10])
plt.subplot(221)
plt.imshow(sample[0][0], cmap='gray')
plt.title("Ground Truth : {}".format(y_test[0]))

sample = X_test[5].reshape((1,1,28,28))
scores = model.predict(sample, verbose=0)
print("Ground Truth "+str(y_test[5]))
print("Predicted "+str(scores))

plt.subplot(222)
plt.imshow(sample[0][0], cmap='gray')
plt.title("Ground Truth : {}".format(y_test[5]))

sample = X_test[10].reshape((1,1,28,28))
scores = model.predict(sample, verbose=0)
print("Ground Truth "+str(y_test[10]))
print("Predicted "+str(scores))

plt.subplot(223)
plt.imshow(sample[0][0], cmap='gray')
plt.title("Ground Truth : {}".format(y_test[10]))

sample = X_test[15].reshape((1,1,28,28))
scores = model.predict(sample, verbose=0)
print("Ground Truth "+str(y_test[15]))
print("Predicted "+str(scores))

plt.subplot(224)
plt.imshow(sample[0][0], cmap='gray')
plt.title("Ground Truth : {}".format(y_test[15]))
plt.show(block=True)